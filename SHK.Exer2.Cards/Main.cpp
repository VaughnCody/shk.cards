// Stephen Kuhn
// 61135 Lab Exercise 2 - Playing Card
// 2/7/2022

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Suit
{
	SPADES = 0,
	DIAMONDS = 1,
	CLUBS = 2,
	HEARTS = 3
};

enum Rank
{
	TWO = 2,
	THREE = 3,
	FOUR = 4,
	FIVE = 5,
	SIX = 6,
	SEVEN = 7,
	EIGHT = 8,
	NINE = 9,
	TEN = 10,
	JACK = 11,
	QUEEN = 12,
	KING = 13,
	ACE = 14
};

struct Card
{
	Suit suit;		// Suit of Card
	Rank CardValue; // Rank Value of Playing Card

};
void PrintCard(Card card)
{
	string value = to_string(card.CardValue);
	string suit = "";
	if (card.CardValue == 11) value = "Jack";
	if (card.CardValue == 12) value = "Queen";
	if (card.CardValue == 13) value = "King";
	if (card.CardValue == 14) value = "Ace";
	
	if (card.suit == 0) suit = "Spades";
	if (card.suit == 1) suit = "Diamond";
	if (card.suit == 2) suit = "Clubs";
	if (card.suit == 3) suit = "Hearts";
	
	cout << "The " << value << " of " << suit << endl;
}

Card HighCard(Card card1, Card card2)
	{
		if (card1.CardValue < card2.CardValue)
		{
			return card2;
		}
		else return card1;
	}
int main()
{
	Card card;
	card.CardValue = JACK;
	card.suit = SPADES;
	
	Card card1;
	Card card2;

	card1.suit = HEARTS;
	card1.CardValue = FOUR;

	card2.suit = CLUBS;
	card2.CardValue = TEN;

	HighCard(card1, card2);
	PrintCard(card);

	return 0;
}